﻿using Microsoft.Win32;
using sharedCode;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Xml.Serialization;

namespace LibraryMusic
{
    /// <summary>
    /// Lógica de interacción para LibraryForm.xaml
    /// </summary>
    public partial class LibraryForm : Window
    {
        private Library lb = null;

        public LibraryForm()
        {
            InitializeComponent();
        }
        public LibraryForm(Library lb)
        {
            InitializeComponent();
            this.lb = lb;
            inputName.Text = lb.Name;
            txtRuta.Text = lb.SavePath;
        }

        private void Guardar_Libreria(object sender, RoutedEventArgs e)
        {
            Library lib = ((StoreData)this.Owner.DataContext).Libraries.save(inputName.Text, txtRuta.Text, lb);
            ((StoreData)this.Owner.DataContext).GuardarLibreria(lib);
            this.Close();
        }

        private void promptLibrary(object sender, RoutedEventArgs e)
        {
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.DefaultExt = "xml";
            if (sfd.ShowDialog() == true)
            {
                Stream myStream = null;
                if ((myStream = sfd.OpenFile()) != null)
                {
                    txtRuta.Text = sfd.FileName;
                    myStream.Close();
                }
            }
        }
    }
}
