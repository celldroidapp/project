﻿using sharedCode;
using SQLite;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

//TODO: Mostrar el titulo actual
//TODO: Poner imagenes
//TODO: Integrar mahapps en el proyecto

namespace LibraryMusic
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    /// 

    enum Modes
    {
        Folder,
        PlayLists
    }

    public partial class MainWindow : Window
    {
        Modes mode = Modes.Folder;
        string currentTitle = "Droid cell music";
        Boolean isPlaying = false;

        public string CurrentTitle
        {
            get
            {
                return currentTitle;
            }
        }

        public MainWindow()
        {
            InitializeComponent();
            StoreData sd =new StoreData();
            libraries_collection.ItemsSource = sd.Libraries;
            this.DataContext = sd;
        }

        private void toggleAside(object sender, MouseButtonEventArgs e)
        {
            aside.Visibility = aside.Visibility == Visibility.Collapsed ? Visibility.Visible : Visibility.Collapsed;
        }
        private Modes this[Modes index]
        {
            get { return this.mode; }
            set {
                if (value == Modes.Folder)
                {
                    folder_view.Visibility = Visibility.Visible;
                    playlist_view.Visibility = Visibility.Collapsed;
                }
                else
                {
                    if (value == Modes.PlayLists)
                    {
                        folder_view.Visibility = Visibility.Collapsed;
                        playlist_view.Visibility = Visibility.Visible;
                    }
                }
            }
        }
        private void closeWindow(object sender, MouseButtonEventArgs e)
        {
            ((StoreData)this.DataContext).GuardarLibrerias();
            this.Close();
        }

        private void restoreWindow(object sender, MouseButtonEventArgs e)
        {
            if (WindowState == WindowState.Normal)
            {
                WindowState = WindowState.Maximized;
            } else
            {
                WindowState = WindowState.Normal;
            }
        }

        private void minimizeWindow(object sender, MouseButtonEventArgs e)
        {
            this.WindowState = WindowState.Minimized;
        }

        private void switchToFolderMode(object sender, MouseButtonEventArgs e)
        {
            this[mode] = Modes.Folder;
        }

        private void switchToPlayListsMode(object sender, MouseButtonEventArgs e)
        {
            this[mode] = Modes.PlayLists;
        }

        private void addLibrary(object sender, MouseButtonEventArgs e)
        {
            LibraryForm lf = new LibraryForm()
            {
                Owner = this
            };
            lf.Show();
        }

        private void RemoveLibrary(object sender, MouseButtonEventArgs e)
        {
            MessageBoxResult mbresult = MessageBox.Show("¿Esta seguro que desea eliminar la libreria?", "Eliminando libreria", MessageBoxButton.YesNo);
            if (mbresult == MessageBoxResult.Yes)
            {
                Library lib = (Library)((Image)sender).DataContext;
                Libraries libraries = ((StoreData)this.DataContext).Libraries;
                libraries.RemoveAt(libraries.IndexOf(lib));
                Console.WriteLine("bbbb");
            }
        }

        private void RenameLibrary(object sender, MouseButtonEventArgs e)
        {
            Library lib = (Library)((Image)sender).DataContext;
            LibraryForm libForm = new LibraryForm(lib)
            {
                Owner = this
            };
            libForm.Show();
        }

        private void libreriaCambiada(object sender, SelectionChangedEventArgs e)
        {
            StoreData sd =(StoreData)this.DataContext;
            Library lib = (Library) libraries_collection.SelectedItem;
            if (lib == null) return;
            ((StoreData)this.DataContext).setDir(lib.SavePath);
            SongDirectory sDir = sd.CurrentPathDir;
            songlist.ItemsSource = sDir.songs;
            folderlist.ItemsSource = sDir.directories;
            upDirBTN.Visibility = Visibility.Collapsed;
            currentDir.Text = sDir.NameDirectory;
        }

        private void changeFolder(object sender, MouseButtonEventArgs e)
        {
            StackPanel ctrl = (StackPanel)sender;
            String dir = (String)ctrl.DataContext;

            StoreData sd = (StoreData)this.DataContext;
            String newDir = System.IO.Path.Combine(new String[] { currentDir.Text, dir });
            sd.setDir(newDir);
            songlist.ItemsSource = sd.CurrentPathDir.songs;
            folderlist.ItemsSource = sd.CurrentPathDir.directories;
            upDirBTN.Visibility = Visibility.Visible;
            currentDir.Text = newDir;
        }

        private void upDirectory(object sender, RoutedEventArgs e)
        {
            StoreData sd = (StoreData)this.DataContext;
            sd.upDirectory(currentDir.Text);
            Library lib = (Library)libraries_collection.SelectedItem;
            String currentDirPath = new FileInfo(lib.SavePath).Directory.FullName;
            currentDir.Text = new FileInfo(currentDir.Text).Directory.FullName;
            if (currentDir.Text == currentDirPath)
            {
                upDirBTN.Visibility = Visibility.Collapsed;
            }
        }
    }
}
