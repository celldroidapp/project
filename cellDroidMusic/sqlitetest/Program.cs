﻿using sharedCode;
using SQLite;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace sqlitetest
{
    class Usuario
    {
        public string Name { get; set; }
        public string SurName { get; set; }
        [SQLite.Indexed]
        public string Email { get; set; }
    }

    public class Book
    {
        public string Title { get; set; }

        public Book()
        {

        }
        public Book(string v)
        {
            this.Title = v;
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            //connectSQLite();
            String pathXML = @"C:\Users\rul\Desktop\musica\fake.xml";
            Library lib = new Library();
            lib.Name = "bbbb";
            lib.SavePath = @"c:\bbb.rxt";
            XmlSerializer xmls = new XmlSerializer(typeof(Library));
            Console.WriteLine("Nombre inicial: " + lib.Name);

            using (StreamWriter sw = new StreamWriter(pathXML))
            {
                xmls.Serialize(sw, lib);
            }

            using (StreamReader sr = new StreamReader(pathXML))
            {
                Library lib2 = (Library) xmls.Deserialize(sr);
                Console.WriteLine("Nombre deserializado: " + lib2.Name);
            }
            /*
            WriteXML();*/

        }

        private static void connectSQLite()
        {
            /*SQLiteConnection dbConn = new SQLiteConnection("db.sqlite");
            dbConn.CreateTable<Usuario>();
            dbConn.Insert(new Usuario() { Name = "raul", SurName = "contrerasd", Email = "raul@email.com" });
            Console.WriteLine("holaaaa");*/

        }

        

        public static void WriteXML()
        {
            Book overview = new Book("bbbb");
            System.Xml.Serialization.XmlSerializer writer =
                new System.Xml.Serialization.XmlSerializer(typeof(Book));

            var path = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "//SerializationOverview.xml";
            System.IO.FileStream file = System.IO.File.Create(path);

            writer.Serialize(file, overview);
            file.Close();
        }
    }
}
