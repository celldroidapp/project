﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace sharedCode
{
    public class Song
    {
        private FileInfo fi;

        public Song(FileInfo fi)
        {
            this.fi = fi;
        }

        [XmlIgnore]
        private TagLib.File tagLibFile {
            get
            {
                return TagLib.File.Create(fi.FullName);
            }
        }
        [XmlIgnore]
        public string Name {
            get
            {
                if (tagLibFile.Tag.Title != null)
                {
                    return tagLibFile.Tag.Title;
                }
                else
                {
                    return fi.Name;
                }
            }
            set
            {
                tagLibFile.Tag.Title = value;
            }
        }
        [XmlIgnore]
        public string Artist {
            get
            {
                return tagLibFile.Tag.FirstPerformer;
            }
            set
            {
                tagLibFile.Tag.Performers = new String[1] { value };
            }
        }
        [XmlIgnore]
        public uint Duration {
            get
            {
                return tagLibFile.Tag.DiscCount;
            }
        }
    }
    public class PlayList
    {
        public string name;
        [XmlArrayItem("songs", typeof(Song)), XmlArray("songs")]
        public ObservableCollection<Song> songs;
    }
    public class SongDirectory
    {
        public ObservableCollection<String> directories { get; set; }
        public ObservableCollection<Song> songs { get; set; }
        public static List<String> extensionsSupported = new List<string> { ".mp3", ".wav" };
        private string path;
        private FileInfo currentDirectoryInfo;

        public SongDirectory(string path)
        {
            directories = new ObservableCollection<string>();
            songs = new ObservableCollection<Song>();
            this.currentDirectoryInfo = new FileInfo(path);
            this.currentDirectory = new FileInfo(path).Directory.FullName;
        }

        public String NameDirectory
        {
            get
            {
                return currentDirectoryInfo.DirectoryName;
            }
        }

        public String FullNameDirectory
        {
            get
            {
                return currentDirectoryInfo.Directory.FullName;
            }
        }

        public string currentDirectory {
            set
            {
                songs.Clear();
                directories.Clear();
                DirectoryInfo di = new DirectoryInfo(value);
                DirectoryInfo[] dirs = di.GetDirectories();
                foreach (DirectoryInfo dir in dirs)
                {
                    directories.Add(dir.Name);
                }
                FileInfo[] files = di.GetFiles();
                foreach (FileInfo fi in files)
                {
                    if (extensionsSupported.IndexOf(fi.Extension) != -1)
                    {
                        songs.Add(new Song(fi));
                    }
                }
                Console.WriteLine("bbb");
            }
        }
    }

    public struct IndexLibrary
    {
        public string name;
        public string savePath;
        private Library lib;

        public IndexLibrary(Library lib) : this()
        {
            this.lib = lib;
        }
    }

    //sqlite
    public class IndexLibraries : ObservableCollection<IndexLibrary>
    {

    }
    public class StoreData
    {
        private IndexLibraries indexLibraries;
        private int currentLibraryIndex;
        private SongDirectory currentPathDir;
        private Libraries libraries;

        public StoreData()
        {
            this.indexLibraries = new IndexLibraries();
            this.currentLibraryIndex = -1;
            this.currentPathDir = null;
            this.libraries = new Libraries();
        }

        public Library currentLibrary{
            get {
                return this.libraries[currentLibraryIndex];
            }
        }

        public void GuardarLibrerias ()
        {
            foreach (Library libreria in libraries)
            {
                GuardarLibreria(libreria);
            }
        }
        public void GuardarLibreria (Library lib)
        {
            using (StreamWriter sw = new StreamWriter(lib.SavePath))
            {
                XmlSerializer xmlSerial = new XmlSerializer(typeof(Library));
                xmlSerial.Serialize(sw, lib);
            }
        }

        public void setDir (string path)
        {
            if (currentPathDir == null)
            {
                this.currentPathDir = new SongDirectory(path);
            } else
            {
                this.currentPathDir.currentDirectory = path;
            }

        }

        public SongDirectory CurrentPathDir
        {
            get
            {
                return CurrentPathDir1;
            }

            set
            {
                CurrentPathDir1 = value;
            }
        }

        public IndexLibraries IndexLibraries
        {
            get
            {
                return indexLibraries;
            }

            set
            {
                indexLibraries = value;
            }
        }

        public void upDirectory(String currentPath)
        {
            this.CurrentPathDir.currentDirectory = new FileInfo(currentPath).Directory.FullName;
        }

        public int CurrentLibrary
        {
            get
            {
                return currentLibraryIndex;
            }

            set
            {
                currentLibraryIndex = value;
            }
        }

        public SongDirectory CurrentPathDir1
        {
            get
            {
                return currentPathDir;
            }

            set
            {
                currentPathDir = value;
            }
        }

        public Libraries Libraries
        {
            get
            {
                return libraries;
            }

            set
            {
                libraries = value;
            }
        }
    }
}
