﻿using SQLite;
using System;
using System.Collections.ObjectModel;
using System.Xml.Serialization;

namespace sharedCode
{
    public class Library
    {
        [XmlAttribute]
        public string Name { get; set; }
        [XmlAttribute, PrimaryKey]
        public string SavePath { get; set; }

        //[XmlIgnore]
        [XmlArrayItem("playlists", typeof(PlayList)), XmlArray("playlists")]
        private ObservableCollection<PlayList> playlists;

        [Ignore, XmlIgnore]
        public ObservableCollection<PlayList> Playlists
        {
            get
            {
                return playlists;
            }

            set
            {
                playlists = value;
            }
        }
    }
    public class Libraries : ObservableCollection<Library> 
    {
        private SQLiteConnection db;

        public Libraries()
        {
            this.db = new SQLiteConnection("dblib.sqlite");
            this.db.CreateTable<Library>();
            TableQuery<Library> libs = db.Table<Library>();
            foreach (Library lib in libs)
            {
              this.Add(lib);
            }
        }
        public Library save (string name, string savePath, Object lb)
        {
            Library lib = new Library() { Name = name, SavePath = savePath};
            if (lb != null)
            {
                this.db.Update(lib);
                this[this.IndexOf((Library)lb)] = lib;
            }
            else
            {
                this.db.Insert(lib);
                Add(lib);
            }
            return lib;
        }
        protected override void RemoveItem(int index)
        {
            db.Delete(this[index]);
            base.RemoveItem(index);
        }
    }
}
